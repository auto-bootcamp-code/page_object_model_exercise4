﻿using OpenQA.Selenium;

namespace Page_Object_Model_Exercise4.PageObjectModel
{
   public class ShoppingCartPage:BasePage
    {
        public ShoppingCartPage(IWebDriver driver) : base(driver)
        {
        }

        private IWebElement shoppingCartPage => Driver.FindElement(By.Id("order"));
        private IWebElement centerColumn => shoppingCartPage.FindElement(By.Id("center_column"));
        private IWebElement productRow => shoppingCartPage.FindElement(By.Id("product_5_19_0_0"));
        public IWebElement CartDeleteButton => productRow.FindElement(By.CssSelector("td.cart_delete a"));
        public IWebElement CartEmptyAlert => centerColumn.FindElement(By.CssSelector("p"));

        public void WaitForCartAlert()
        {
            Wait.Until(driver => CartEmptyAlert.Displayed);
        }

        public void WaitForShoppingCartPage()
        {
           Wait.Until(driver => shoppingCartPage.Displayed);
        }
    }
}
