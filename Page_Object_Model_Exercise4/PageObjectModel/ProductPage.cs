﻿using OpenQA.Selenium;

namespace Page_Object_Model_Exercise4.PageObjectModel
{
   public class ProductPage:MainPage
    {
        public ProductPage(IWebDriver driver) : base(driver)
        {
        }

        private const char dollarSign = '$';
        private const char fullStop = '.';
        private const char comma = ',';
        private IWebElement productPage => Driver.FindElement(By.Id("product"));
        private IWebElement emailForm => productPage.FindElement(By.Id("send_friend_form_content"));
        private IWebElement centerColumn => PageRoot.FindElement(By.Id("center_column"));
        public IWebElement CartForm => centerColumn.FindElement(By.CssSelector("div.box-cart-bottom"));
        public IWebElement CartButton => CartForm.FindElement(By.Name("Submit"));
        public IWebElement CartModal => productPage.FindElement(By.Id("layer_cart"));
        public IWebElement CheckoutButton => CartModal.FindElement(By.CssSelector("a"));
        public IWebElement CloseCartModal => CartModal.FindElement(By.CssSelector("span"));
        public IWebElement SendFriendButton => productPage.FindElement(By.Id("send_friend_button"));
        public IWebElement FriendName => emailForm.FindElement(By.Id("friend_name"));
        public IWebElement FriendEmail => emailForm.FindElement(By.Id("friend_email"));
        public IWebElement SendEmailButton => productPage.FindElement(By.Id("sendEmail"));
        public IWebElement SendEmailError => productPage.FindElement(By.Id("send_friend_form_error"));
        public IWebElement EmailSentModal => productPage.FindElement(By.CssSelector("div.fancybox-wrap  p"));
        public IWebElement ProductPrice => productPage.FindElement(By.Id("our_price_display"));
        public IWebElement ProductQuantity => productPage.FindElement(By.Id("layer_cart_product_quantity"));
        public IWebElement ProductTotal => productPage.FindElement(By.Id("layer_cart_product_price"));

        public void WaitForCartForm()
        {
          Wait.Until(driver => CartForm.Displayed);
        }
        
        public void WaitForCartModal()
        {
            Wait.Until(driver => CartModal.Displayed);
        }

        public void WaitForEmailForm()
        {
            Wait.Until(driver => emailForm.Displayed);
        }

        public void WaitForEmailSentModal()
        {
            Wait.Until(driver => EmailSentModal.Displayed);
        }

        public void WaitForErrorMessage()
        {
            Wait.Until(driver => SendEmailError.Displayed);
        }
        
        public string GetTotal(string productPrice)
        {
            productPrice = productPrice.Split(dollarSign)[1];
            var price = double.Parse(productPrice.Replace(fullStop, comma));
            var quantity = double.Parse(ProductQuantity.Text);
            var total = string.Concat(dollarSign, (price * quantity)).Replace(comma, fullStop);
            
            return total;
        }
    }
}
