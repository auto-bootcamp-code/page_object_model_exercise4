﻿using OpenQA.Selenium;

namespace Page_Object_Model_Exercise4.PageObjectModel
{
   public static class StoreApp
    {
        public static MainPage MainPage;
        public static SearchPage SearchPage;
        public static ShoppingCartPage ShoppingCartPage;
        public static ProductPage ProductPage;

        public static void Init(IWebDriver driver)
        {
            driver.Navigate().GoToUrl("http://automationpractice.com/index.php");
            MainPage = new MainPage(driver);
            ShoppingCartPage = new ShoppingCartPage(driver);
            SearchPage = new SearchPage(driver);
            ProductPage = new ProductPage(driver);
        }
    }
}
