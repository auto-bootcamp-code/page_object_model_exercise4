﻿using OpenQA.Selenium;
using System.Collections.Generic;

namespace Page_Object_Model_Exercise4.PageObjectModel
{
    public class SearchPage:MainPage
    {
        public SearchPage(IWebDriver driver) : base(driver)
        {
        }

        public IList<IWebElement> SearchedItemNames => CenterColumn.FindElements(By.CssSelector("a.product-name"));

        public void WaitForResults()
        {
            Wait.Until(driver => SearchedItemNames.Count > 0);
        }

        public void SelectProduct()
        {
            SearchedItemNames[0].Click();
        }

        public void Search(string itemName)
        {
            SearchInput.SendKeys(itemName);
        }
    }
}

