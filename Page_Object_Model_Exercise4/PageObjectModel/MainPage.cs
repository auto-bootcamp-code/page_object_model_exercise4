﻿using OpenQA.Selenium;

namespace Page_Object_Model_Exercise4.PageObjectModel
{
    public class MainPage:BasePage
    {
        public MainPage(IWebDriver driver) : base(driver)
        { 
        }

        private IWebElement searchBox => PageRoot.FindElement(By.Id("searchbox"));
        public IWebElement SearchInput => searchBox.FindElement(By.Name("search_query"));
        public IWebElement SearchButton => searchBox.FindElement(By.Name("submit_search"));
        public IWebElement CenterColumn => PageRoot.FindElement(By.Id("center_column"));
        public IWebElement SelenuimButton => PageRoot.FindElement(By.CssSelector("#cmsinfo_block a"));

        public string GetUrl()
        {
            return Driver.Url;
        }
    }
}
