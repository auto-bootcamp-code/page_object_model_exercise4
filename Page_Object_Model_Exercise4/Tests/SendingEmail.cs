﻿using NUnit.Framework;
using Page_Object_Model_Exercise4.PageObjectModel;

namespace Page_Object_Model_Exercise4.Tests
{
    [TestFixture]
    public class SendingEmail : Base
    {
        private const string friendName = "Kwenza";
        private const string email = "kg@gmail.com";

        [SetUp]
        public new void Setup()
        {
            StoreApp.SearchPage.Search(itemName);
            StoreApp.SearchPage.SearchButton.Click();
            StoreApp.SearchPage.WaitForResults();
            StoreApp.SearchPage.SelectProduct();
            StoreApp.ProductPage.WaitForCartForm();
            StoreApp.ProductPage.SendFriendButton.Click();
            StoreApp.ProductPage.WaitForEmailForm();
        }

        [Test]
        public void GivenValidInput_WhenSendingAnEmail_ThenDisplaySuccessModal()
        {
            StoreApp.ProductPage.FriendName.SendKeys(friendName);
            StoreApp.ProductPage.FriendEmail.SendKeys(email);
            StoreApp.ProductPage.SendEmailButton.Click();
            StoreApp.ProductPage.WaitForEmailSentModal();

            Assert.IsTrue(StoreApp.ProductPage.EmailSentModal.Displayed);
        }

        [Test]
        public void GivenEmptyFields_WhenSendingAnEmail_ThenDisplayValidationMessage()
        {
            StoreApp.ProductPage.SendEmailButton.Click();
            StoreApp.ProductPage.WaitForErrorMessage();

            Assert.IsTrue(StoreApp.ProductPage.SendEmailError.Displayed);
        }
    }
}
