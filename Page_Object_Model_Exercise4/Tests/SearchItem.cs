﻿using NUnit.Framework;
using Page_Object_Model_Exercise4.PageObjectModel;

namespace Page_Object_Model_Exercise4.Tests
{
    [TestFixture]
    public class SearchItem : Base
    {
        private const string newUrl = "http://www.seleniumframework.com/";
        
        [Test]
        public void GivenAnExistingItemName_WhenSearching_ThenReturnItemCountGreaterThanZero()
        {
            StoreApp.SearchPage.Search(itemName);
            StoreApp.SearchPage.SearchButton.Click();
            StoreApp.SearchPage.WaitForResults();

            Assert.IsTrue(StoreApp.SearchPage.SearchedItemNames.Count > 0);
        }

        [Test]
        public void GivenLinkButton_WhenRedirecting_ThenConfirmTheNewUrl()
        {
            StoreApp.MainPage.SelenuimButton.Click();

            Assert.AreEqual(StoreApp.MainPage.GetUrl(),newUrl);
        }
    }
}
