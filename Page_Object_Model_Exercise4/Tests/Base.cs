﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.IO;
using System.Reflection;
using Page_Object_Model_Exercise4.PageObjectModel;

namespace Page_Object_Model_Exercise4.Tests
{
    [TestFixture]
    public class Base
    {
        private IWebDriver driver;
        private const int driverWaitTime=15;
        public const string itemName = "Dress";
        [SetUp]
        public void Setup()
        {
            var chromeOptions = new ChromeOptions();
            chromeOptions.AddArguments("--start-maximized");
            driver = new ChromeDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), chromeOptions);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(driverWaitTime);
            StoreApp.Init(driver);
        }

        [TearDown]
        public void CleanUp()
        {
            driver.Dispose();
        }
    }
}
